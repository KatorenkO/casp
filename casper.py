import sqlite3
import os
from flask import Flask, render_template, request, g, flash, abort, redirect, url_for
from DB import DB
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, login_user, login_required, logout_user, current_user
from UserLogin import UserLogin

DATABASE = '/tmp/casper.db'
DEBUG = True
SECRET_KEY = 'fdgfh78@#ui89ugyuihiojk5?>gfhf89dx,v06k'

app = Flask(__name__)
app.config.from_object(__name__)
app.config.update(dict(DATABASE=os.path.join(app.root_path,'casper.db')))

login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message = "Авторизуйтесь для доступа к закрытым страницам"
login_manager.login_message_category = "success"

@login_manager.user_loader
def load_user(user_id):
    print("load_user")
    return UserLogin().fromDB(user_id, dbase)

def connect_db():
    conn = sqlite3.connect(app.config['DATABASE'])
    conn.row_factory = sqlite3.Row
    return conn

def create_db():
    db = connect_db()
    with app.open_resource('db.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()
    db.close()

def get_db():
    if not hasattr(g, 'link_db'):
        g.link_db = connect_db()
    return g.link_db

dbase = None
@app.before_request
def before_request():
    global dbase
    db = get_db()
    dbase = DB(db)

@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'link_db'):
        g.link_db.close()

@app.route("/")
def index():
    return render_template('main.html')

@app.route("/auto", methods=["POST", "GET"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('profile'))    
    if request.method == "POST":
        user = dbase.getUserByEmail(request.form['username'])
        if user and check_password_hash(user['psw'], request.form['psw']):
            userlogin = UserLogin().create(user)
            rm = True if request.form.get('remainme') else False
            login_user(userlogin, remember=rm)
            return redirect(request.args.get("next") or url_for("profile"))
        flash("Неверный логин и/или пароль", "error")

    return render_template("auto.html", title="Authorization")

@app.route("/registration", methods=["POST", "GET"])
def reg():
    if request.method == "POST":
        if len(request.form['username']) > 0 and len(request.form['mail']) > 6 and request.form['psw'] == request.form['r_psw']:
            hash = generate_password_hash(request.form['psw'])
            res = dbase.addUser(request.form['username'], request.form['mail'], hash)
            if res:
                flash("Вы успешно зарегистрированы", "success")
            else:
                flash("Ошибочка", "error")
        elif request.form['psw'] != request.form['r_psw']:
            flash("Пароли не совпадают", "error")
        elif len(request.form['username']) == 0:
            flash("Не заполнено поле логина", "error")
        else:
            flash("Слишком короткий e-mail. Он должен состоять более, чем из 6 символов", "error")

    return render_template("reg.html", title="Registration")

@app.errorhandler(401)
@app.errorhandler(404)
def pageNotFount(error):
    return render_template('page404.html', title="Страница не найдена")

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash("Вы вышли из аккаунта", "success")
    return redirect(url_for('index'))

@app.route('/profile')
@login_required
def profile():
    return f"""<a href="{url_for('logout')}">Разлогиниться</a>
                <p>user info: {current_user.get_id()}"""

if __name__ == "__main__":
    app.run(debug=True)